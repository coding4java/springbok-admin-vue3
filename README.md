<p align="center">
 <img src="https://img.shields.io/badge/Vue-3.4-blue.svg" alt="Downloads">
</p>

## 项目介绍

springbok的前端项目，基于Vue3、Element-Plus、Vite的快速开发平台，简洁、灵活、结构清晰，遵循Apache2.0协议，完全开源，可免费用于个人或商业项目，点击上方Star，关注更新

### 后端项目

[https://gitee.com/coding4java/springbok](https://gitee.com/coding4java/springbok)

## 文档

[《springbok 开发文档》](https://www.yuque.com/fengwensheng-ot86q/rm6qf3?#)

## 技术栈

| 框架                 | 版本    | 描述                    |
| -------------------- | ------- | ----------------------- |
| Vue                  | 3.4.15  | 渐进式JavaScript框架    |
| Vite                 | 5.0.11  | 开发与构建工具          |
| TypeScript           | 5.3.0   | JavaScript类型的超集    |
| Element-Plus         | 2.5.5   | UI组件库                |
| Pinia                | 2.1.7   | Vue状态管理库           |
| Pinia-Plugin-Persist | 1.0.0   | Pinia持久化存储插件     |
| Vue Router           | 4.2.5   | Vue路由                 |
| Axios                | 1.6.7   | 基于Promise的网络请求库 |
| Echarts              | 5.4.3   | 可视化图表库            |
| Dayjs                | 1.11.10 | 日期操作工具库          |
| Wangeditor           | 5.1.23  | 富文本编辑器            |

## 项目结构

```lua
springbok-admin-vue3
├── src
│   ├── api         -- api接口文件
│   ├── assets      -- 样式文件
│   ├── components  -- 全局组件 
│   ├── router      -- 路由
│   ├── store       -- pinia store
│   ├── utils       -- 工具包
│   ├── views       -- 页面文件
```

## 运行项目

1.前端环境配置

安装node

```shell
# 安装node后，检查node当前版本
node -v
```

2.安装依赖

进入前端项目根目录，执行下面命令安装依赖

```shell
npm i
```

3.启动前端项目

进入项目根目录，执行下面命令运行前端项目

```shell
npm run dev
```

4.启动完成后，访问`http://localhost:5173`，默认用户名：`admin`，密码：`123456`