import request from "@/utils/request";


export function pageItem(data) {
  return request({
    url: "/item/pageItem",
    method: "GET",
    params: data,
  });
}
export function addItem(data) {
  return request({
    url: "/item/addItem",
    method: "POST",
    data,
  });
}
export function updateItem(data) {
  return request({
    url: "/item/updateItem",
    method: "POST",
    data,
  });
}
export function deleteItem(id) {
  return request({
    url: "/item/deleteItem",
    method: "POST",
    params: { id },
  });
}
