import request from "@/utils/request";

export function pageCouponDirect(data) {
  return request({
    url: "/couponDirect/pageCouponDirect",
    method: "GET",
    params: data,
  });
}
export function selectCouponDirectById(couponDirectId) {
  return request({
    url: "/couponDirect/selectCouponDirectById",
    method: "GET",
    params: { couponDirectId },
  });
}
export function addCouponDirect(data) {
  return request({
    url: "/couponDirect/addCouponDirect",
    method: "POST",
    data,
  });
}
export function updateCouponDirect(data) {
  return request({
    url: "/couponDirect/updateCouponDirect",
    method: "POST",
    data,
  });
}
export function deleteCouponDirect(couponDirectId) {
  return request({
    url: "/couponDirect/deleteCouponDirect",
    method: "POST",
    params: { couponDirectId },
  });
}
export function sendCoupon(couponDirectId) {
  return request({
    url: "/couponDirect/sendCoupon",
    method: "POST",
    params: { couponDirectId },
  });
}
