import request from "@/utils/request";

export function pageItemSaleChannel(data) {
  return request({
    url: "/itemSaleChannel/pageItemSaleChannel",
    method: "GET",
    params: data,
  });
}
export function addItemSaleChannel(data) {
  return request({
    url: "/itemSaleChannel/addItemSaleChannel",
    method: "POST",
    data,
  });
}
export function updateItemSaleChannel(data) {
  return request({
    url: "/itemSaleChannel/updateItemSaleChannel",
    method: "POST",
    data,
  });
}
export function deleteItemSaleChannel(ids) {
  return request({
    url: "/itemSaleChannel/deleteItemSaleChannel",
    method: "POST",
    params: { ids },
  });
}
