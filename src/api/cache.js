import request from "@/utils/request";

export function listCacheIndex() {
  return request({
    url: "/cache/listCacheIndex",
    method: "GET",
  });
}
