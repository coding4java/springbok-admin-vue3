import request from "@/utils/request";

export function pageTableColumn(data) {
  return request({
    url: "/tableColumn/pageTableColumn",
    method: "GET",
    params: data,
  });
}
export function addTableColumn(data) {
  return request({
    url: "/tableColumn/addTableColumn",
    method: "POST",
    data,
  });
}
export function updateTableColumn(data) {
  return request({
    url: "/tableColumn/updateTableColumn",
    method: "POST",
    data,
  });
}
export function deleteTableColumn(id) {
  return request({
    url: "/tableColumn/deleteTableColumn",
    method: "POST",
      params: { id },
  });
}
