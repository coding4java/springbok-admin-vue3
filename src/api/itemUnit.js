import request from "@/utils/request";


export function pageItemUnit(data) {
  return request({
    url: "/itemUnit/pageItemUnit",
    method: "GET",
    params: data,
  });
}
export function listItemUnit(data) {
  return request({
    url: "/itemUnit/listItemUnit",
    method: "GET",
    params: data,
  });
}
export function addItemUnit(data) {
  return request({
    url: "/itemUnit/addItemUnit",
    method: "POST",
    data,
  });
}
export function updateItemUnit(data) {
  return request({
    url: "/itemUnit/updateItemUnit",
    method: "POST",
    data,
  });
}
export function deleteItemUnit(id) {
  return request({
    url: "/itemUnit/deleteItemUnit",
    method: "POST",
    params: { id },
  });
}
