import request from "@/utils/request";

export function pageTable(data) {
  return request({
    url: "/table/pageTable",
    method: "GET",
    params: data,
  });
}
export function selectTableById(id) {
  return request({
    url: "/table/selectTableById",
    method: "GET",
    params: { id },
  });
}
export function addTable(data) {
  return request({
    url: "/table/addTable",
    method: "POST",
    data,
  });
}
export function updateTable(data) {
  return request({
    url: "/table/updateTable",
    method: "POST",
    data,
  });
}
export function deleteTable(id) {
  return request({
    url: "/table/deleteTable",
    method: "POST",
    params: { id },
  });
}
export function generateCode(id) {
  return request({
    url: "/table/generateCode",
    method: "GET",
    params: { id },
    responseType: "blob",
  });
}
